import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'
import App from './views/app.vue'

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDb9lFtofhMUXe7YIu7g7uyB4_E5BUbOLY',
    libraries: 'places',
  }
});

new Vue({
  render: h => h(App),
}).$mount('#app')